# coding=utf-8
from selenium import webdriver
from time import sleep

hate_names = ['Анна', 'Anna', 'Coco', 'Helga','Bridgitte', 'Charly','Annabell','Laura','Susanne']


class TinderBot:
    def __init__(self):
        self.driver = webdriver.Chrome()

    def login(self):
        self.driver.get('https://tinder.com')

        sleep(2)

        fb_button = self.driver.find_element_by_xpath(
            '//*[@id="modal-manager"]/div/div/div/div/div[3]/div[2]/button/span')
        fb_button.click()

        main_window = self.driver.window_handles[0]
        self.driver.switch_to_window(self.driver.window_handles[1])

        email = self.driver.find_element_by_xpath('//*[@id="email"]')
        email.send_keys('arkadiymaestro@gmail.com')

        psw = self.driver.find_element_by_xpath('//*[@id="pass"]')
        psw.send_keys('Qopfx345ABK378')

        submit_btn = self.driver.find_element_by_xpath('//*[@id="u_0_0"]')
        submit_btn.click()

        self.driver.switch_to_window(main_window)
        sleep(2)

        allow_loc = self.driver.find_element_by_xpath('//*[@id="modal-manager"]/div/div/div/div/div[3]/button[1]')
        allow_loc.click()

        sleep(1)

        enable_notif = self.driver.find_element_by_xpath(
            '//*[@id="modal-manager"]/div/div/div/div/div[3]/button[2]')
        enable_notif.click()

    def close_notifs(self):
        notif = self.driver.find_element_by_xpath('//*[@id="modal-manager"]/div/div/div[2]/button[2]')
        notif.click()

    def skip_match(self):
        match = self.driver.find_element_by_xpath('//*[@id="modal-manager-canvas"]/div/div/div[1]/div/div[3]/a')
        match.click()

    def close_tinder_plus(self):
        popup = self.driver.find_element_by_xpath('//*[@id="modal-manager"]/div/div/div[3]/button[2]')
        popup.click()

    def like(self):
        name = ''
        like = self.driver.find_element_by_xpath(
            '//*[@id="content"]/div/div[1]/div/main/div[1]/div/div/div[1]/div/div[2]/button[3]')
        try:
            girl_name_element = self.driver.find_element_by_xpath(
                '//*[@id="content"]/div/div[1]/div/main/div[1]/div/div/div[1]/div/div[1]/div[3]/div[6]/div/div[1]/div/div/span')
            name = girl_name_element.text
            if name in hate_names:
                print name
                self.dislike()
                return
        except Exception:
            print Exception
        like.click()
        sleep(1)

    def dislike(self):
        dislike = self.driver.find_element_by_xpath(
            '//*[@id="content"]/div/div[1]/div/main/div[1]/div/div/div[1]/div/div[2]/button[1]')
        dislike.click()
        sleep(1)

    def start_liking(self):
        while True:
            try:
                self.like()
            except Exception:
                try:
                    self.close_notifs()
                except Exception:
                    try:
                        self.skip_match()
                    except Exception:
                        self.close_tinder_plus()


bot = TinderBot()
bot.login()
bot.start_liking()
