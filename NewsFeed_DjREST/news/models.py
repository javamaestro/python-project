from django.db import models


# Create your models here.
class News(models.Model):
    title = models.CharField(max_length=100, blank=True, default='')
    content = models.CharField(max_length=100, blank=True, default='')
    author = models.CharField(max_length=100, blank=True, default='')
    created = models.DateTimeField(auto_now_add=True)
    file = models.FileField(blank=False, null=True, upload_to="")


class Comments(models.Model):
    new = models.ForeignKey(News, on_delete=models.CASCADE)
    comment_created = models.DateTimeField(auto_now_add=True)
    comment_content = models.CharField(max_length=100, blank=True, default='')
    comment_author = models.CharField(max_length=100, blank=True, default='')


