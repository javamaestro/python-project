from django.urls import path, include

from . import views

urlpatterns = [
    path('news/', views.NewsView.as_view(), name='news_list'),
    path('newsDetails/<int:pk>', views.NewsDetails.as_view(), name='news_details'),
    path('comments/', views.CommentsView.as_view(), name='comments_list'),
    path('commentsDetails/<int:pk>', views.CommentsDetails.as_view())
]
