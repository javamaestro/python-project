from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from news.models import *
from news.serializers import *
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from api import settings


class NewsView(APIView):
    pagination_class = settings.DEFAULT_PAGINATION_CLASS

    def get(self, request, *args, format=None):
        news_details = News.objects.all()
        page = request.GET.get('page')
        pageSize = request.GET.get('size')
        paginator = Paginator(news_details, pageSize)
        try:
            news_details = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            news_details = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            news_details = paginator.page(paginator.num_pages)
        serializer = NewsSerializer(news_details, many=True)
        return Response(serializer.data)

    def post(self, request, *args, **kwargs):
        post_serializer = NewsSerializer(data=request.data)
        if post_serializer.is_valid():
            post_serializer.save()
            return Response(post_serializer.data, status=status.HTTP_201_CREATED)
        else:
            print('error', post_serializer.errors)
            return Response(post_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class NewsDetails(APIView):
    def get_object(self, pk):
        try:
            return News.objects.get(pk=pk)
        except News.DoesNotExist:
            return Response('error', status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, pk, format=None):
        new = self.get_object(pk)
        serializer = NewsSerializer(new)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        new = self.get_object(pk)
        serializer = NewsSerializer(new, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        new = self.get_object(pk)
        new.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class CommentsView(APIView):
    def post(self, request):
        post_serializer = CommentSerializer(data=request.data)
        if post_serializer.is_valid():
            post_serializer.save()
            return Response(post_serializer.data, status=status.HTTP_201_CREATED)
        else:
            print('error', post_serializer.errors)
            return Response(post_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CommentsDetails(APIView):
    def get(self, request, pk, format=None):
        print(pk)
        comments = Comments.objects.filter(new_id=pk)
        serializer = CommentSerializer(comments, many=True)
        return Response(serializer.data)
