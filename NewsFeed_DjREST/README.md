

Step 1:

Installing virtualenv $ python3 -m pip install --user virtualenv

Creating a virtual environment $ python3 -m venv env

Activating a virtual environment $ source env/bin/activate

Using requirements files $ pip install -r requirements.txt

Step 2:

Before creating a project, you need to initialize the models in the database

$ python manage.py makemigrations

$ python manage.py migrate

To run the application, use the command (port can be changed)

$ python manage.py runserver 0:8000

