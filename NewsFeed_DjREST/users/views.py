import requests
import yagmail
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from api import settings
from .models import User
from .serializers import CreateUserSerializer

CLIENT_ID = 12345
CLIENT_SECRET = 12345


@api_view(['POST'])
@permission_classes([AllowAny])
def verificate(request):
    user = User.objects.get(email=request.data['email'])
    if user.get_verification_code() == request.data['code']:
        user.set_is_email_verified()
    user.save()


@api_view(['POST'])
@permission_classes([AllowAny])
def register(request):
    serializer = CreateUserSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        user = User.objects.get(email=request.data['email'])
        yag_smtp_connection = yagmail.SMTP(user='news.feed@inbox.ru', password='EXm78C72r;MU>&C*',
                                           host=settings.EMAIL_HOST)
        subject = 'Hello from richard'
        contents = ['Hello, can you please verify you mail with this verification code: ', user.get_verification_code()]
        yag_smtp_connection.send(user.get_email(), subject, contents)
        yag_smtp_connection.close()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    else:
        print('error', serializer.errors)
    return Response(serializer.errors)


@api_view(['POST'])
@permission_classes([AllowAny])
def login(request):
    user = User.objects.get(username=request.data['username'])
    print(user)
    if user.get_is_email_verified():
        r = requests.post('http://localhost:8000/o/token/',
                          data={
                              'grant_type': 'password',
                              'username': request.data['username'],
                              'password': request.data['password'],
                              'client_id': CLIENT_ID,
                              'client_secret': CLIENT_SECRET,
                          },
                          )
        data = r.json()
        data.update({'username': request.data['username']})
        return Response(data)
    else:
        return Response('Email is not verified', status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
@permission_classes([AllowAny])
def token(request):
    '''
    Gets tokens with username and password. Input should be in the format:
    {"username": "username", "password": "1234abcd"}
    '''
    r = requests.post(
        'http://localhost:8000/o/token/',
        data={
            'grant_type': 'password',
            'username': request.data['username'],
            'password': request.data['password'],
            'client_id': CLIENT_ID,
            'client_secret': CLIENT_SECRET,
        },
    )
    return Response(r.json())


@api_view(['POST'])
@permission_classes([AllowAny])
def refresh_token(request):
    '''
    Registers user to the server. Input should be in the format:
    {"refresh_token": "<token>"}
    '''
    r = requests.post(
        'http://localhost:8000/o/token/',
        data={
            'grant_type': 'refresh_token',
            'refresh_token': request.data['refresh_token'],
            'client_id': CLIENT_ID,
            'client_secret': CLIENT_SECRET,
        },
    )
    return Response(r.json())


@api_view(['POST'])
@permission_classes([AllowAny])
def revoke_token(request):
    '''
    Method to revoke tokens.
    {"token": "<token>"}
    '''
    r = requests.post(
        'http://localhost:8000/o/revoke_token/',
        data={
            'token': request.data['token'],
            'client_id': CLIENT_ID,
            'client_secret': CLIENT_SECRET,
        },
    )
    # If it goes well return sucess message (would be empty otherwise)
    if r.status_code == requests.codes.ok:
        return Response({'message': 'token revoked'}, r.status_code)
    # Return the error if it goes badly
    return Response(r.json(), r.status_code)
