from api import settings
from django.db import models
from django.contrib.auth.models import AbstractUser

class User(AbstractUser):
    is_email_verified = models.BooleanField(default=False)
    verification_code = models.CharField(max_length=5, blank=False)

    def set_vc(self, value):
        self.verification_code = value

    def get_verification_code(self):
        return self.verification_code

    def set_is_email_verified(self):
        self.is_email_verified = True

    def get_is_email_verified(self):
        return self.is_email_verified

    def get_email(self):
        return self.email

