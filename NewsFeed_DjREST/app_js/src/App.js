import React, { Component } from 'react';
import News from './components/News'
import Login from './components/Login'


export default class MainPage extends Component {

  render() {
    console.log(Auth.authenticate());
    console.log(Auth.isAuthenticated);
    Auth.authenticate();
    return (
      <div>

        <div class="container">
          <div class="row">
            <div class="col">
              <News />
            </div>
            {Auth.isAuthenticated ? (
              <div class="col col-lg-2">
                Hello, {localStorage.getItem("fullName")}!
              <AuthButton />
              </div>) :
              (<div class="col col-lg-2">
               <Login />
              </div>)}
          </div>
        </div>
      </div>
    );
  }
}

const Auth = {
  isAuthenticated: false,
  authenticate() {
    if (localStorage.getItem('access_token')) {
      if(localStorage.getItem('access_token') == 'undefined'){
        return false;
      }
      this.isAuthenticated = true;
    }
  },

  signout() {
    localStorage.clear();
    this.isAuthenticated = false;
    window.location.href = "/";
  }
}

const AuthButton = (
  ({ history }) =>
    Auth.isAuthenticated ? (
      <p>
        <button type="button" class="btn btn-danger"
          onClick={() => {
            Auth.signout(() => history.push("/"));
          }}
        >
          Sign out
        </button>
      </p>
    ) : (
        <p>You are not logged in.</p>
      )
);
