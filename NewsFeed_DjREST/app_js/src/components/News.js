import React from 'react';
import axios from 'axios';
import Comment from './Comment'
export default class News extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      title: '',
      content: '',
      selectedFile: '',
      feeds: [],
      pageSize: '',
      totalPages: '',
      pageSize: 10,
      totalPages: '',
      page: 0,
    };
    this.handleTitleChange = this.handleTitleChange.bind(this);
    this.handleContentChange = this.handleContentChange.bind(this);
    this.handleFileChange = this.handleFileChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.loadNewsFromServer = this.loadNewsFromServer.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleNews = this.handleNews.bind(this);
    this.handleNews2 = this.handleNews2.bind(this);
    this.handleNews3 = this.handleNews3.bind(this);
    this.loadFirstPage = this.loadFirstPage.bind(this);
    this.loadLastPage = this.loadLastPage.bind(this);
    this.loadPrevPage = this.loadPrevPage.bind(this);
    this.loadNextPage = this.loadNextPage.bind(this);
  }

  handleTitleChange(e) {
    e.preventDefault();
    this.setState({ title: e.target.value });
  }

  handleContentChange(e) {
    e.preventDefault();
    this.setState({ content: e.target.value });
  }

  handleFileChange(e) {
    e.preventDefault();
    this.setState({ selectedFile: e.target.files[0] });
  }

  getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
      var cookies = document.cookie.split(';');
      for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i].trim();
        // Does this cookie string begin with the name we want?
        if (cookie.substring(0, name.length + 1) === (name + '=')) {
          cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
          break;
        }
      }
    }
    return cookieValue;
  }

  handleDelete(id) {
    axios.delete('http://localhost:8000/api/newsDetails/' + id, {
      headers: {
        'Authorization': 'bearer ' + localStorage.getItem("access_token")
      }
    })
      .then((resp) => {
        this.loadNewsFromServer(this.state.pageSize, this.state.page);
      })
  }

  handleSubmit(event) {
    event.preventDefault();
    const data = new FormData();
    data.append('title', this.state.title);
    data.append('content', this.state.content);
    data.append('file', this.state.selectedFile);
    data.append('author', localStorage.getItem("fullName"))
    var csrftoken = getCookie('csrftoken');
    axios.post('http://localhost:8000/api/news/', data,
      {
        headers: {
          'Content-Type': 'multipart/form-data',
          'X-CSRFToken': csrftoken,
          'Authorization': 'bearer ' + localStorage.getItem("access_token")

        }
      })
      .then((response) => {
        this.loadNewsFromServer(this.state.pageSize, this.state.page);
      });
  }

  loadNewsFromServer(pageSize, page) {
    axios.get('http://localhost:8000/api/news/', {
      params: {
        'size': pageSize,
        'page': page
      }
      ,
      headers: {
        "Content-Type": "application/json",
        'Authorization': 'bearer ' + localStorage.getItem("access_token")
      }
    })
      .then((response) => {
        console.log(response);
        this.setState({
          feeds: response.data
        })
      }).catch((err) => {
        console.log(err);
      })
  }

  componentDidMount() {
    this.loadNewsFromServer(this.state.pageSize, this.state.page);
  }

  loadLastPage() {
    var page = this.state.totalPages - 1;
    var pageSize = this.state.pageSize;
    this.setState({ page: page });
    this.loadNewsFromServer(pageSize, page);
  }

  loadFirstPage() {
    var page = 0;
    var pageSize = this.state.pageSize;
    this.setState({ page: page });
    this.loadNewsFromServer(pageSize, page);
  }

  loadPrevPage() {
    var pageSize = this.state.pageSize;
    var page = this.state.page;
    if (page > 0 && page <= this.state.totalPages - 1) {
      page -= 1;
      this.setState({ page: page });
    } else {
      this.loadNewsFromServer(pageSize, 0);
    }
    this.loadNewsFromServer(pageSize, page);
  }

  loadNextPage() {
    var pageSize = this.state.pageSize;
    var page = this.state.page;
    if (page < (this.state.totalPages - 1) && page >= 0) {
      page += 1;
      this.setState({ page: page });
    } else {
      page = this.state.totalPages - 1;
      this.loadNewsFromServer(pageSize, page);
    }
    this.loadNewsFromServer(pageSize, page);
  }

  loadLastPage() {
    var page = this.state.totalPages - 1;
    var pageSize = this.state.pageSize;
    this.setState({ page: page });
    this.loadNewsFromServer(pageSize, page);
  }

  handleNews() {
    var pageSize = "5";
    var page = "0";
    this.setState({ pageSize: pageSize });
    this.loadNewsFromServer(pageSize, page);
  }

  handleNews2() {
    var pageSize = "10";
    var page = "0";
    this.setState({ pageSize: pageSize });
    this.loadNewsFromServer(pageSize, page);
  }

  handleNews3() {
    var pageSize = "15";
    var page = "0";
    this.setState({ pageSize: pageSize });
    this.loadNewsFromServer(pageSize, page);
  }

  render() {
    return (
      <div class="full-background">
        <header>
          <div class="bg">
            <h1 align="center">Welcome to News Feed!</h1>
          </div>
        </header>
        <div>
            <label>The number of feeds per page</label>
            <br />
            <div class="btn-group" role="group" aria-label="Basic example">
              <button type="button" class="btn btn-secondary" onClick={this.handleNews} >5</button>
              <button type="button" class="btn btn-secondary" onClick={this.handleNews2} >10</button>
              <button type="button" class="btn btn-secondary" onClick={this.handleNews3} >15</button>
            </div>
            <br />
          </div>
        <div>
          <form>
            <h2>Adding feed</h2>
            <div >
              <label>Title</label>
              <br />
              <input type="text" name="title" onChange={this.handleTitleChange} />
            </div>
            <div>
              <label>Content</label>
              <br />
              <textarea rows="5" cols="70" name="content" onChange={this.handleContentChange}></textarea>
            </div>
            <div>
              <input type="file" class="btn btn-light" accept="image/png, image/jpeg" name="file" id="file" placeholder="Select a file for upload" onChange={this.handleFileChange} />
            </div>
            <div>
              <button class="btn btn-success" type="button" onClick={this.handleSubmit}>Add..</button>
            </div>
          </form>
        </div>
        < br />
        <div>
          <label>News feed: </label>
          <FeedList feeds={this.state.feeds} handleDelete={this.handleDelete} />
          <nav aria-label="Page navigation example">
          <ul class="pagination">
            <li class="page-item"><a class="page-link" onClick={this.loadFirstPage}>1</a></li>
            <li class="page-item"><a class="page-link" onClick={this.loadPrevPage}>2</a></li>
            <li class="page-item"><a class="page-link" onClick={this.loadNextPage}>3</a></li>
            <li class="page-item"><a class="page-link" onClick={this.loadLastPage}>4</a></li>
          </ul>
        </nav>
        </div>
      </div >
    );
  }
}

class FeedList extends React.Component {
  render() {
    const feeds = this.props.feeds.map((feed) =>
      <Feed key={feed.id} feed={feed} handleDelete={this.props.handleDelete} />
    );
    return (
      <div>
        <div class="feed">
          {feeds}
        </div>
        <div>
        </div>
      </div>
    )
  }
}

class Feed extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      titleEdit: '',
      contentEdit: '',
      fId: ''
    };
    this.deleteNew = this.deleteNew.bind(this);
  }

  deleteNew(id) {
    this.props.handleDelete(this.props.feed.id);
  }

  render() {
    var path = this.props.feed.file;
    return (
      <div class="blog-post">
        <h3 class="ptitile">{this.props.feed.title}</h3>
        <h6 align="right">{this.props.feed.created}</h6>
        <h6>{this.props.feed.content}</h6>
        <h6 align="right">Author: {this.props.feed.author}</h6>
        <img src={"http://localhost:8000" + path} />
        <br />
        <i align="right" class="fas fa-times fa-2x" onClick={this.deleteNew}></i>
        <Comment feed={this.props.feed} />
        <hr />
      </div>
    );
  }
}

function getCookie(name) {
  var cookieValue = null;
  if (document.cookie && document.cookie !== '') {
    var cookies = document.cookie.split(';');
    for (var i = 0; i < cookies.length; i++) {
      var cookie = cookies[i].trim();
      // Does this cookie string begin with the name we want?
      if (cookie.substring(0, name.length + 1) === (name + '=')) {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break;
      }
    }
  }
  return cookieValue;
}