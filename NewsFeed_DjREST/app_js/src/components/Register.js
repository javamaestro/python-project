import React from 'react';
import axios from 'axios';


export default class Registration extends React.Component {
  constructor(props) {
    super(props);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleFullNameChange = this.handleFullNameChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      email: '',
      password: '',
      username: ''
    };
  }

  handleEmailChange(e) {
    this.setState({ email: e.target.value })
  }
  handlePasswordChange(e) {
    this.setState({ password: e.target.value })
  }
  handleFullNameChange(e) {
    this.setState({ username: e.target.value })
  }

  handleSubmit(event) {
    event.preventDefault();
    axios.post('http://localhost:8000/authentication/register/',
      {
        email: this.state.email,
        password: this.state.password,
        username: this.state.username,
        grant_type: "password",
        client_id: 12345,
        client_secret: 54321,
      }, {
      'Content-Type': 'application/json',
    }
    ).then((response) => {
      window.location.href = "/verify";
    });
  }

  render() {
    return (
      <div>
        <form>
          Registration:
          <div class="form-group">
            <label>Email address</label>
            <input type="email" placeholder="Enter your email" name="email" onChange={this.handleEmailChange} />
            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
          </div>
          <div class="form-group">
            <label>Password</label>
            <input type="password" placeholder="Enter your password" name="password" onChange={this.handlePasswordChange} />
          </div>
          <div class="form-group">
            <label>Full name</label>
            <input type="text" placeholder="Enter your password" name="full name" onChange={this.handleFullNameChange} />
          </div>
          <button  onClick={this.handleSubmit}>Registration</button>
        </form>
      </div>
    )
  }
}
