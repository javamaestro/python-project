import React from 'react';
import { Button, View, StyleSheet } from 'react-native';
import Register from './Register';

import axios from 'axios';

export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.handleUsernameChange = this.handleUsernameChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      username: '',
      password: '',
      redirect: false,
    };
  }

  handleUsernameChange(e) {
    this.setState({ username: e.target.value })
  }
  handlePasswordChange(e) {
    this.setState({ password: e.target.value })
  }

  setRedirect = () => {
    this.setState({
      redirect: true
    })
  }

  handleSubmit(event) {
    event.preventDefault();
    axios.post('http://localhost:8000/authentication/login/',
      {
        grant_type: "password",
        username: this.state.username,
        password: this.state.password,
        client_id: 12345,
        client_secret: 12345,

      }, {
      'Content-Type': 'application/x-www-form-urlencoded',
    }
    ).then((response) => {
      console.log("responce", response);
      localStorage.setItem("access_token", response.data.access_token);
      localStorage.setItem("refresh_token", response.data.refresh_token);
      localStorage.setItem("fullName", response.data.username);
      Auth.authenticate();
      window.location.href = "/";


    });
  }

  render() {
    return (
      <div>
        <form>
          Authorization:
          <div class="form-group">
            <label>Email address</label>
            <input type="text" placeholder="Enter your email" name="email" onChange={this.handleUsernameChange} />
            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
          </div>
          <div class="form-group">
            <label>Password</label>
            <input type="password" placeholder="Enter your password" name="password" onChange={this.handlePasswordChange} />
          </div>
          <button class="btn btn-success" onClick={this.handleSubmit}>Sign In</button> or
           <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
            Register new account
            </a>
          <div class="collapse" id="collapseExample">
            <div class="card card-body">
              <Register />
            </div>
          </div>
        </form>
      </div>
    )
  }
}

const Auth = {
  isAuthenticated: false,
  authenticate() {
    if (localStorage.getItem('access_token')) {
      this.isAuthenticated = true;
    }
  },

  signout() {
    localStorage.clear();
    this.isAuthenticated = false;
    window.location.href = "/";
  }
}



