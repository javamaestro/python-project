import React, { Component } from 'react';
import { Collapse, Button, CardBody, Card } from 'reactstrap';
import axios, * as others from 'axios';

class Comm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      commentContent: '',
      author: '',
      feed: '',
      comments: [],
      isAdmin: false,
      selectedFile: null,
    };
    this.toggle = this.toggle.bind(this);
    this.state = { collapse: false };
    this.state = { arrowOpen: false };
    this.state = { className: "fa fa-arrow-down" };
    this.handleCommentSender = this.handleCommentSender.bind(this);
    this.handleComment = this.handleComment.bind(this);
    this.handleFileChange = this.handleFileChange.bind(this);
    this.handleLoadComments = this.handleLoadComments.bind(this);
    this.commDelete = this.commDelete.bind(this);
  }

  toggle(e) {
    var open = false;
    this.setState(state => ({ collapse: !state.collapse }));
    if (this.state.arrowOpen) {
      this.setState(state => ({ className: "fa fa-arrow-down" }));
    } else {
      this.setState(state => ({ className: "fa fa-arrow-down open" }));
    }
    this.setState(state => ({ arrowOpen: !state.arrowOpen }));
  }

  handleComment(e) {
    this.setState({ commentContent: e.target.value })
  }

  handleFileChange(eve) {
    this.setState({ selectedFile: eve.target.files[0] })
  }

  commDelete(comment) {
    axios.delete('http://localhost:8000/api/comments/' + comment.id)
      .then((response) => {
        this.handleLoadComments();
      });
  }

  handleCommentSender(event) {
    event.preventDefault();
    const data = new FormData();
    data.append('comment_content', this.state.commentContent);
    data.append('comment_author', localStorage.getItem("fullName"));
    data.append('new', this.props.feed.id)
    axios.post('http://localhost:8000/api/comments/', data,
      {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'bearer ' + localStorage.getItem("access_token")

        }
      }).then((response) => {
        this.handleLoadComments();
      });
  }

  componentDidMount() {
    this.handleLoadComments();
  }

  handleLoadComments() {
    axios.get('http://localhost:8000/api/commentsDetails/' + this.props.feed.id, {
      headers: {
        'Authorization': 'bearer ' + localStorage.getItem("access_token")
      }
    })
      .then((response) => {
        console.log(response);
        this.setState({
          comments: response.data
        });
      }).catch((error) => {
        console.log(error);
      })
  }

  render() {
    return (
      <div>
        <div id="container-arrow">
          <i id="icon" class={this.state.className} onClick={this.toggle} style={{ marginBottom: '1rem' }}></i>
        </div>
        <Collapse isOpen={this.state.collapse}>
          <Card>
            <CardBody>
              <form>
                <h6>Comment additionad</h6>
                <div>
                  <label>Text</label>
                  <br />
                  <textarea rows="5" cols="30" name="content" onChange={this.handleComment}></textarea>
                </div>
                <div>
                  <button type="button" onClick={this.handleCommentSender}>Add</button>
                </div>
              </form>
              <div>
                {this.state.comments ? (
                  <CommentList commDelete={this.commDelete} comments={this.state.comments} changeRating={this.changeRating} returnRating={this.returnRating} />
                ) : (
                    <div>Loading comments</div>
                  )}
              </div>
            </CardBody>
          </Card>
        </Collapse>
      </div>
    );
  }
}

class CommentList extends React.Component {
  render() {
    const comments = this.props.comments.map((comment) =>
      <Comment key={comment.id} comment={comment} commDelete={this.props.commDelete} changeRating={this.props.changeRating} returnRating={this.props.returnRating} />
    );
    return (
      <div>
        <div>
          <h2>Comment List</h2>
          {comments}
        </div>
        <div>
        </div>
      </div>
    )
  }
}

class Comment extends React.Component {
  constructor(props) {
    super(props);
    this.handleCommDelete = this.handleCommDelete.bind(this);
  }

  handleCommDelete() {
    this.props.commDelete(this.props.comment);
  }



  render() {
    return (
      <div>
        <p>{this.props.comment.comment_content}</p>
        <p align="right">Author: {this.props.comment.comment_author}</p>
        <h6 align="right">{this.props.comment.comment_created}</h6>
        <br />
        <hr />
      </div >
    )
  }
}


export default Comm;
