import React from 'react';
import axios from 'axios';


export default class Registration extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      code: ''
    };
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handleCodeChange = this.handleCodeChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

  }

  handleEmailChange(e) {
    this.setState({ email: e.target.value })
  }
  handleCodeChange(e) {
    this.setState({ code: e.target.value })
  }

  handleSubmit(event) {
    event.preventDefault();
    axios.post('http://localhost:8080/authentication/verify',
      {
        email: this.state.email,
        code: this.state.code
      }, {
      'Content-Type': 'application/json',
    }
    )
      .then((response) => {
        window.location.href = "/login";
      });
  }

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <label>
            Person email:
                     <input type="text" name="email" onChange={this.handleEmailChange} />
          </label>
          <br />
          <label>
            Verification Code:
                    <input type="text" name="VerificationCode" onChange={this.handleCodeChange} />
          </label>
          <button>Send verification</button>
        </form>
      </div>
    )
  }
}
